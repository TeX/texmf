#!/bin/sh

# Find the directories in the structure
structure=$(find "submodules" -mindepth 3 -type d -not -name .dummy)

# Make a list of directories
dirs=$(echo "${structure}" | perl -pe 's/submodules\/[\w-]+\/(.*)\/[\w-]+/$1/' | sort -u)

# Create directory structure
mkdir -p ${dirs}

# Create symlinks
for f in ${structure}; do
    # Create link destination
    l=$(echo "$f" | perl -pe 's/submodules\/[\w-]+\///')

    # Create symlink if it doesn't exist
    if [ ! -e "$l" ]; then
        echo "Creating: $f => $l"
        ln -rs "$f" "$l"

	# Create dummy directories for Kpathsea
	# See: https://tex.stackexchange.com/questions/132908/fontspec-search-path-in-texmfhome
	ddir="$(dirname "$l")/.dummy"
	mkdir -p "${ddir}"
	touch "${ddir}/.keep"
    fi
done
